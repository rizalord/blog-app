<a href="{{ route('articles.show', $featuredBlog->blog->slug) }}">
    <div class="w-full lg:max-w-full lg:flex border border-gray-200 rounded">
        @if ($featuredBlog->blog->image)
        <div class="h-48 lg:h-auto lg:w-48 flex-none bg-cover rounded-t lg:rounded-t-none lg:rounded-l text-center overflow-hidden"
            style="background-image: url('{{ asset('storage/' . $featuredBlog->blog->image) }}')"
            title="Woman holding a mug">
        </div>
        @endif
        <div class="bg-white p-4 flex flex-col justify-between leading-normal">
            <div class="mb-8">
                <div class="text-gray-900 font-bold text-xl mb-2 line-clamp-2">{{ $featuredBlog->blog->title }}</div>
                <p class="text-gray-700 text-base line-clamp-3">{{ trim($featuredBlog->blog->description) }}</p>
            </div>
            <div class="flex items-center">
                <img class="w-10 h-10 rounded-full mr-4"
                    src="https://ui-avatars.com/api/?name={{ $featuredBlog->blog->author->name }}"
                    alt="{{ $featuredBlog->blog->author->name }}">
                <div class="text-sm">
                    <p class="text-gray-900 leading-none">{{ $featuredBlog->blog->author->name }}</p>
                    <p class="text-gray-600">{{ $featuredBlog->blog->created_at->format('d M Y') }}</p>
                </div>
            </div>
        </div>
    </div>
</a>