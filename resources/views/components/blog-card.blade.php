<div class="w-full md:max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
    <a href="{{ route('articles.show', $blog->slug) }}">
        @if ($blog->image)
        <img class="object-cover rounded-t-lg w-full aspect-video" src="{{ asset('storage/'.$blog->image) }}" alt="{{ $blog->title }}">
        @endif
    </a>
    <div class="p-5">
        <a href="{{ route('articles.show', $blog->slug) }}">
            <h5 class="mb-2 text-2xl font-bold tracking-tight text-gray-900 dark:text-white line-clamp-2">
                {{ $blog->title }}
            </h5>
        </a>
        <p class="mb-8 font-normal text-gray-700 dark:text-gray-400 line-clamp-3">
            {{ trim($blog->description) }}
        </p>
        <div class="flex items-center space-x-4">
            <img class="w-10 h-10 rounded-full" src="https://ui-avatars.com/api/?name={{ $blog->author->name }}"
                alt="{{ $blog->author->name }}">
            <div class="font-medium dark:text-white">
                <div>{{ $blog->author->name }}</div>
                <div class="text-sm text-gray-500 dark:text-gray-400">
                    {{ $blog->created_at->format('d F Y') }}
                </div>
            </div>
        </div>
    </div>
</div>