<x-app-layout>
    <x-slot:title>
        {{ __('Dashboard') }}
    </x-slot:title>

    <div class="container">
        <div class="card">
            <div class="card-header">Dashboard</div>
            <div class="card-body">
                Welcome to dashboard.
            </div>
        </div>
    </div>

</x-app-layout>