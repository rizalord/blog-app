<x-blog-layout>
    <div class="flex flex-col gap-8">
        @if ($blog->currentPage() == 1 && $featuredBlogs->isNotEmpty())
        <section id="featured">
            <h1 class="text-2xl font-bold mb-3">Featured</h1>
            <hr class="mb-3" />

            <ul class="grid grid-cols-1 lg:grid-cols-2 gap-5">
                <li>
                    @foreach ($featuredBlogs as $data)
                    <x-blog-card-horizontal :featuredBlog="$data" />
                    @endforeach
                </li>
            </ul>
        </section>
        @endif

        <div class="grid grid-cols-12 gap-5">
            <section id="articles" class="col-span-12 md:col-span-8">
                <h1 class="text-2xl font-bold mb-3">All Articles</h1>
                <hr class="mb-3" />

                @if ($blog->isEmpty())
                <div class="flex flex-col items-center justify-center py-12">
                    <h1 class="text-2xl font-bold mb-3">No Articles</h1>
                    <p class="text-gray-500">There are no articles yet.</p>
                </div>
                @endif

                <ul class="grid grid-cols-1 sm:grid-cols-2 gap-5">
                    @foreach ($blog as $data)
                    <li>
                        <x-blog-card :blog="$data" />
                    </li>
                    @endforeach
                </ul>
            </section>

            <section id="filter" class="col-span-12 md:col-span-4">
                <h1 class="text-2xl font-bold mb-3">Categories</h1>
                <hr class="mb-3" />

                <div
                    class="w-full text-sm font-medium md:text-lg text-gray-900 bg-white border border-gray-200 rounded-lg dark:bg-gray-700 dark:border-gray-600 dark:text-white">
                    @foreach ($categories as $category)
                    <a href="{{ route('articles.category', $category->slug) }}" class=" block w-full px-4 py-4 border-b border-gray-200 cursor-pointer hover:bg-gray-100
                        hover:text-blue-700 focus:outline-none focus:ring-2 focus:ring-blue-700 focus:text-blue-700
                        dark:border-gray-600 dark:hover:bg-gray-600 dark:hover:text-white dark:focus:ring-gray-500
                        dark:focus:text-white">
                        {{ $category->name }}
                    </a>
                    @endforeach
                </div>
            </section>
        </div>

        <section id="pagination">
            {{ $blog->links() }}
        </section>

    </div>
</x-blog-layout>