<x-blog-layout>
    <x-slot:title>
        {{ __('Category') }} {{ $category->name }}
    </x-slot:title>

    <div class="flex flex-col gap-5">

        <section id="articles">
            <h1 class="text-2xl font-bold mb-3">Category {{ $category->name }}</h1>
            <hr class="mb-3" />

            @if ($articles->isEmpty())
            <div class="flex flex-col items-center justify-center py-12">
                <h1 class="text-2xl font-bold mb-3">No Articles</h1>
                <p class="text-gray-500">There are no articles in this category.</p>
            </div>
            @endif

            <ul class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-5">
                @foreach ($articles as $data)
                <li>
                    <x-blog-card :blog="$data" />
                </li>
                @endforeach
            </ul>
        </section>

        <section id="pagination">
            {{ $articles->links() }}
        </section>

    </div>
</x-blog-layout>