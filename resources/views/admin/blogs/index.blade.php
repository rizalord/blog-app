<x-app-layout>
    <x-slot:title>
        {{ __('Blogs') }}
    </x-slot:title>

    <x-slot:styles>
        <link href="https://cdn.datatables.net/v/bs5/jq-3.7.0/dt-1.13.6/r-2.5.0/datatables.min.css" rel="stylesheet">
    </x-slot:styles>

    <div class="container">
        <div class="card">
            <div class="card-header">Manage Blogs</div>
            <div class="card-body">
                @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
                @endif

                <div class="d-flex justify-content-end">
                    <a href="{{ route('blogs.create') }}" class="btn btn-primary mb-3">Create Blog</a>
                </div>

                <form action="{{ route('blogs.index') }}" method="GET" class="mb-4" id="form-filter">
                    <div class="row">
                        <div class="col-md-3">
                            <select name="category_id" id="category_id" class="form-control">
                                <option value="" {{ request('category_id') ? '' : 'selected' }}>-- Select Category
                                    @foreach ($categories as $category)
                                <option value="{{ $category->id }}" {{ request('category_id')==$category->id ?
                                    'selected' : '' }}
                                    >{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>

                <div class="table-responsive">
                    {{ $dataTable->table() }}
                </div>
            </div>
        </div>
    </div>

    <x-slot:scripts>
        <script src="https://cdn.datatables.net/v/bs5/jq-3.7.0/dt-1.13.6/r-2.5.0/datatables.min.js"></script>

        <script>
            $(document).ready(function() {
                $('#form-filter').on('change', function() {
                    $('#form-filter').submit();
                });
            });
        </script>

        @if(request('category_id'))
        {{ $dataTable->ajax(route('blogs.index', ['category_id' => request('category_id')]))->scripts(attributes:
        ['type' => 'module']) }}
        @else
        {{ $dataTable->ajax(route('blogs.index'))->scripts(attributes: ['type' => 'module']) }}
        @endif

    </x-slot:scripts>
</x-app-layout>