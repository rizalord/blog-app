<x-app-layout>

    <x-slot:title>
        {{ __('Detail Blog') }}
    </x-slot:title>

    <x-slot:styles>
        <script src="{{ asset('vendor/ckeditor5/build/ckeditor.js') }}"></script>
    </x-slot:styles>

    <div class="container">
        <div class="card">
            <div class="card-header">Detail Blog</div>
            <div class="card-body">

                <div class="row">
                    <div class="col-md-8 mb-3">
                        <label for="title">Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title"
                            name="title" value="{{ old('title', $blog->title) }}" disabled>
                        @error('title')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="col-md-4 mb-3">
                        <div class="form-group">
                            <label for="category_id">Category</label>
                            <input type="text" class="form-control @error('category_id') is-invalid @enderror"
                                id="category_id" name="category_id"
                                value="{{ old('category_id', $blog->category->name) }}" disabled>
                            @error('category_id')
                            <div class="invalid-feedback">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="image">Image</label>
                        @if ($blog->image)
                        <br />
                        <img src="{{ asset('storage/' . $blog->image) }}" alt="{{ $blog->title }}" width="100%">
                        @else
                        <span>
                            No image
                        </span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 mb-3">
                        <label for="content">Content</label>
                        <textarea class="form-control @error('content') is-invalid @enderror" id="content"
                            name="content">{{ old('content', $blog->content) }}</textarea>

                        @error('content')
                        <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>
                </div>
            </div>
        </div>
    </div>

    <x-slot:scripts>
        <script>
            ClassicEditor
                        .create( document.querySelector( '#content' ) )
                        .then( editor => {
                            editor.enableReadOnlyMode('editor')
                        })
                        .catch( error => {
                            console.error( error );
                        });
        </script>
    </x-slot:scripts>

</x-app-layout>