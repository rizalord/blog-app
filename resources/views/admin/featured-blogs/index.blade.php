<x-app-layout>
    <x-slot:title>
        {{ __('Featured Blogs') }}
    </x-slot:title>

    <x-slot:styles>
        <link href="https://cdn.datatables.net/v/bs5/jq-3.7.0/dt-1.13.6/r-2.5.0/datatables.min.css" rel="stylesheet">
    </x-slot:styles>

    <div class="container">
        <div class="card">
            <div class="card-header">Manage Featured Blogs</div>
            <div class="card-body">
                @if (session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
                @endif

                <div class="d-flex justify-content-end">
                    <a href="{{ route('featured-blogs.create') }}" class="btn btn-primary mb-3">Add Featured Blog</a>
                </div>

                <div class="table-responsive">
                    {{ $dataTable->table() }}
                </div>
            </div>
        </div>
    </div>

    <x-slot:scripts>
        <script src="https://cdn.datatables.net/v/bs5/jq-3.7.0/dt-1.13.6/r-2.5.0/datatables.min.js"></script>
        
        {{ $dataTable->ajax(route('featured-blogs.index'))->scripts(attributes: ['type' => 'module']) }}
    </x-slot:scripts>
</x-app-layout>