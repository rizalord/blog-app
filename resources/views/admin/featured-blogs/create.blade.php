<x-app-layout>
    <x-slot:title>
        {{ __('Add Featured Blog') }}
    </x-slot:title>

    <div class="container">
        <div class="card">
            <div class="card-header">Add Featured Blog</div>
            <div class="card-body">
                <form method="POST" action="{{ route('featured-blogs.store') }}">
                    @csrf
                    @method('POST')

                    <div class="row">

                        <div class="col-md-4 mb-3">
                            <div class="form-group">
                                <label for="blog_id">Blog</label>
                                <select class="form-control @error('blog_id') is-invalid @enderror" id="blog_id"
                                    name="blog_id">
                                    <option value="">Select Blog</option>
                                    @foreach ($blogs as $blog)
                                    <option value="{{ $blog->id }}" {{ old('blog_id')==$blog->id ? 'selected' : '' }}>{{
                                        $blog->title }}</option>
                                    @endforeach
                                </select>
                                @error('blog_id')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                    </div>

                    <div class="d-flex justify-content-end">
                        <button type="submit" class="btn btn-primary">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</x-app-layout>