<x-blog-layout>
    <x-slot:title>
        {{ $blog->title }}
    </x-slot:title>

    <main class="pt-8 pb-16 lg:pt-16 lg:pb-24 bg-white dark:bg-gray-900">
        <div class="flex justify-between px-4 mx-auto max-w-screen-xl ">
            <section
                class="mx-auto w-full max-w-4xl format format-sm sm:format-base lg:format-lg format-blue dark:format-invert">
                <header class="mb-4 lg:mb-6 not-format">
                    <address class="flex items-center mb-6 not-italic">
                        <div class="inline-flex items-center mr-3 text-sm text-gray-900 dark:text-white">
                            <img class="mr-4 w-16 h-16 rounded-full"
                                src="https://ui-avatars.com/api/?name={{ $blog->author->name }}&color=7F9CF5&background=EBF4FF"
                                alt="{{ $blog->author->name }}">
                            <div>
                                <h6 href="#" rel="author" class="text-xl font-bold text-gray-900 dark:text-white">
                                    {{ $blog->author->name }}</h6>
                                </h6>
                                <p class="text-base font-light text-gray-500 dark:text-gray-400"><time pubdate
                                        datetime="2022-02-08" title="February 8th, 2022">
                                        {{ $blog->created_at->format('M d, Y') }}
                                    </time></p>
                            </div>
                        </div>
                    </address>

                    @if ($blog->image)
                    <img class="w-full aspect-video mb-10 rounded-lg" src="{{ asset('storage/' . $blog->image) }}"
                        alt="{{ $blog->title }}" />

                    @endif

                    <h1
                        class="mb-4 text-3xl font-extrabold leading-tight text-gray-900 lg:mb-6 lg:text-4xl dark:text-white">
                        {{ $blog->title }}
                    </h1>
                </header>

                <article class="prose lg:prose-xl">
                    {!! $blog->content !!}
                </article>

            </section>

        </div>
    </main>
</x-blog-layout>