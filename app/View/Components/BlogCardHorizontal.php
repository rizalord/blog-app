<?php

namespace App\View\Components;

use App\Models\FeaturedBlog;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class BlogCardHorizontal extends Component
{
    public FeaturedBlog $featuredBlog;
    /**
     * Create a new component instance.
     */
    public function __construct(FeaturedBlog $featuredBlog)
    {
        $this->featuredBlog = $featuredBlog;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.blog-card-horizontal');
    }
}
