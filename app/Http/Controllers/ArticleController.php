<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Category;

class ArticleController extends Controller
{
    public function category(Category $category)
    {
        $articles = $category->blogs()->orderBy('created_at', 'desc')->paginate(9);
        return view('articles', compact('articles', 'category'));
    }

    public function show(Blog $blog)
    {
        return view('detail-article', compact('blog'));
    }
}
