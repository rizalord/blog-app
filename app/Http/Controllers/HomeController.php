<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\FeaturedBlog;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $featuredBlogs = !$request->has('page') || $request->page == 1
            ? FeaturedBlog::limit(4)->get() 
            : [];
        $blog = Blog::orderBy('created_at', 'desc')->paginate(6);
        
        return view('home', compact('blog', 'featuredBlogs'));
    }
}
