<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\FeaturedBlogsDataTable;
use App\Http\Controllers\Controller;
use App\Models\FeaturedBlog;
use App\Http\Requests\StoreFeaturedBlogRequest;
use App\Http\Requests\UpdateFeaturedBlogRequest;
use App\Models\Blog;

class FeaturedBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(FeaturedBlogsDataTable $dataTable)
    {
        return $dataTable->render('admin.featured-blogs.index');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $blogs = Blog::all();

        return view('admin.featured-blogs.create', compact('blogs'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFeaturedBlogRequest $request)
    {
        FeaturedBlog::create($request->validated());

        return redirect()->route('featured-blogs.index')
            ->with('success', 'Featured blog created successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FeaturedBlog $featuredBlog)
    {
        $blogs = Blog::all();

        return view('admin.featured-blogs.edit', compact('featuredBlog', 'blogs'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFeaturedBlogRequest $request, FeaturedBlog $featuredBlog)
    {
        $featuredBlog->update($request->validated());

        return redirect()->route('featured-blogs.index')
            ->with('success', 'Featured blog updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(FeaturedBlog $featuredBlog)
    {
        $featuredBlog->delete();

        return redirect()->route('featured-blogs.index')
            ->with('success', 'Featured blog deleted successfully.');
    }
}
