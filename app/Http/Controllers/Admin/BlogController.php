<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\BlogsDataTable;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Http\Requests\StoreBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use App\Models\Category;
use App\Models\FeaturedBlog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(BlogsDataTable $dataTable, Request $request)
    {
        $categories = Category::all();
        return $dataTable->render('admin.blogs.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::all();

        return view('admin.blogs.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBlogRequest $request)
    {
        $image = $request->file('image')->store('blogs');
        $slug = Str::slug($request->title);

        Blog::create([
            'title' => $request->title,
            'slug' => $slug,
            'image' => $image,
            'description' => $request->description,
            'content' => $request->content,
            'category_id' => $request->category_id,
            'author_id' => auth()->user()->id,
        ]);

        return redirect()->route('blogs.index')->with('success', 'Blog created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Blog $blog)
    {
        return view('admin.blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Blog $blog)
    {
        $categories = Category::all();

        return view('admin.blogs.edit', compact('blog', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBlogRequest $request, Blog $blog)
    {
        $slug = Str::slug($request->title);
        $image = $request->hasFile('image')
            ? $request->file('image')->store('blogs')
            : $blog->image;

        if ($image && $blog->image) {
            Storage::delete($blog->image);
        }

        $blog->update([
            'title' => $request->title,
            'slug' => $slug,
            'image' => $image,
            'content' => $request->content,
            'description' => $request->description,
            'category_id' => $request->category_id,
        ]);

        return redirect()->route('blogs.index')->with('success', 'Blog updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Blog $blog)
    {
        FeaturedBlog::where('blog_id', $blog->id)->delete();
        $blog->delete();

        return redirect()->route('blogs.index')->with('success', 'Blog deleted successfully');
    }
}
