<?php

use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\FeaturedBlogController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name("home");
Route::get('/articles/{blog:slug}', [ArticleController::class, 'show'])->name('articles.show');
Route::get('/category/{category:slug}', [ArticleController::class, 'category'])->name('articles.category');

Route::prefix('dashboard')->middleware(['auth', 'verified'])->group(function(){
    Route::get('/', fn() => view('dashboard'))->name('dashboard');
    Route::resource('categories', CategoryController::class);
    Route::resource('blogs', BlogController::class);
    Route::resource('featured-blogs', FeaturedBlogController::class);
});

require __DIR__ . '/auth.php';

Auth::routes();
